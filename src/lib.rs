extern "C" {
    pub fn trace(s: *const ::std::os::raw::c_char);
    pub fn tracef(f: f32);
    pub fn tracei(i: ::std::os::raw::c_int);
}

pub type UpdateFunc = extern "C" fn();

extern "C" {
    pub fn notify_on_update(f: UpdateFunc);
    pub fn remove_update(f: UpdateFunc);
    pub fn get_object(name: *const ::std::os::raw::c_char) -> ::std::os::raw::c_int;
    pub fn set_transform(
        object: ::std::os::raw::c_int,
        x: f32,
        y: f32,
        z: f32,
        rx: f32,
        ry: f32,
        rz: f32,
        sx: f32,
        sy: f32,
        sz: f32,
    );
    pub fn set_location(object: ::std::os::raw::c_int, x: f32, y: f32, z: f32);
    pub fn set_scale(object: ::std::os::raw::c_int, x: f32, y: f32, z: f32);
    pub fn set_rotation(object: ::std::os::raw::c_int, x: f32, y: f32, z: f32);
    pub fn mouse_x() -> ::std::os::raw::c_int;
    pub fn mouse_y() -> ::std::os::raw::c_int;
    pub fn mouse_started(button: ::std::os::raw::c_int) -> ::std::os::raw::c_int;
    pub fn mouse_down(button: ::std::os::raw::c_int) -> ::std::os::raw::c_int;
    pub fn mouse_released(button: ::std::os::raw::c_int) -> ::std::os::raw::c_int;
    pub fn key_started(key: ::std::os::raw::c_int) -> ::std::os::raw::c_int;
    pub fn key_down(key: ::std::os::raw::c_int) -> ::std::os::raw::c_int;
    pub fn key_released(key: ::std::os::raw::c_int) -> ::std::os::raw::c_int;
    pub fn time_real() -> f32;
    pub fn time_delta() -> f32;
    pub fn js_eval(fn_: *const ::std::os::raw::c_char);
    pub fn js_call_object(object: ::std::os::raw::c_int, fn_: *const ::std::os::raw::c_char);
    pub fn js_call_static(path: *const ::std::os::raw::c_char, fn_: *const ::std::os::raw::c_char);
}

